using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ExtensionLibrary
{
    /// <summary>
    /// A helper class to read structured files (tab, comma, pipe delimited).
    /// </summary>
    public class DelimitedFileReader
    {
        /// <summary>
        /// The delimiter to use for splitting columns.
        /// </summary>
        public char Delimiter { get; set; }
        /// <summary>
        /// The text qualifier, if any.
        /// </summary>
        public char? Qualifier { get; set; }

        /// <summary>
        /// Creates a new instance of the StructuredFileReader class. Uses a tab as the delimiter and no text qualifier.
        /// </summary>
        public DelimitedFileReader() : this('\t', null)
        { }

        /// <summary>
        /// Creates a new instance of the StructuredFileReader class.
        /// </summary>
        /// <param name="Delimiter">The delimiter to use for splitting columns.</param>
        public DelimitedFileReader(char Delimiter) : this(Delimiter, null)
        { }

        /// <summary>
        /// Creates a new instance of the StructuredFileReader class.
        /// </summary>
        /// <param name="Delimiter">The delimiter to use for splitting columns.</param>
        /// <param name="Qualifier">The text qualifier, if any.</param>
        public DelimitedFileReader(char Delimiter, char? Qualifier)
        {
            this.Delimiter = Delimiter;
            this.Qualifier = Qualifier;
        }

        /// <summary>
        /// Reads delimited data from the specified file.
        /// </summary>
        /// <param name="FilePath">The file to read.</param>
        /// <param name="ColumnRow">An optional delimited string that contains the columns. Used if the input file doesn't have a header row.</param>
        /// <param name="CaseSensitive">Indicates if the returned Dictionary should have case-sensitive keys.</param>
        /// <param name="CharactersRead">An optional progress reporter. Incrementally reports the number of characters read.</param>
        /// <returns>Returns a collection of dictionaries.</returns>
        public IEnumerable<IDictionary<string, string>> ReadLines(string FilePath, string ColumnRow = null, bool CaseSensitive = true, IProgress<long> CharactersRead = null)
        {
            foreach (var line in ReadLines(File.ReadLines(FilePath), ColumnRow, CaseSensitive, CharactersRead))
            {
                yield return line;
            }
        }

        /// <summary>
        /// Converts the specified collection of strings to delimited data.
        /// </summary>
        /// <param name="Lines">The collection of strings to transform.</param>
        /// <param name="ColumnRow">An optional delimited string that contains the columns. Used if the input file doesn't have a header row.</param>
        /// <param name="CaseSensitive">Indicates if the returned Dictionary should have case-sensitive keys.</param>
        /// <param name="CharactersRead">An optional progress reporter. Incrementally reports the number of characters read.</param>
        /// <returns>Returns a collection of dictionaries.</returns>
        public IEnumerable<IDictionary<string, string>> ReadLines(IEnumerable<string> Lines, string ColumnRow = null, bool CaseSensitive = true, IProgress<long> CharactersRead = null)
        {
            Dictionary<int, string> columns = null;
            var splitter = new DelimitedLineSplitter(Delimiter, Qualifier);
            foreach (var line in Lines)
            {
                CharactersRead?.Report(line.Length);
                //First iteration of foreach
                if (columns == null)
                {
                    //ColumnRow is null. This means first line is column row. Don't process it.
                    if (string.IsNullOrWhiteSpace(ColumnRow))
                    {
                        columns = GetColumns(line, splitter);
                    }
                    //Column row is not null, use to get columns. This means first line is data. Process it.
                    else
                    {
                        columns = GetColumns(ColumnRow, splitter);
                        var values = splitter.SplitLine(line, Delimiter, Qualifier);
                        yield return GetDictionary(columns, values, CaseSensitive);
                    }
                }
                else
                {
                    var values = splitter.SplitLine(line, Delimiter, Qualifier);
                    yield return GetDictionary(columns, values, CaseSensitive);
                }
            }

        }

        /// <summary>
        /// Gets a collection of columns from the specified ColumnRow.
        /// </summary>
        /// <param name="ColumnRow">The column row to split.</param>
        /// <param name="Splitter">The line splitter to use for splitting the columns.</param>
        /// <returns>Returns a dictionary containing the column index and the column value.</returns>
        private Dictionary<int, string> GetColumns(string ColumnRow, DelimitedLineSplitter Splitter)
        {
            return Splitter.SplitLine(ColumnRow, Delimiter, Qualifier).Select((value, index) => new { value = value.Replace(Environment.NewLine, ", ").Trim(), index }).ToDictionary(z => z.index, z => z.value);
        }

        private IDictionary<string, string> GetDictionary(IDictionary<int, string> Columns, List<string> Values, bool CaseSensitive)
        {

            if (CaseSensitive)
            {
                return Columns.ToDictionary(z => z.Value, z => Values[z.Key].Trim());
            }
            else
            {
                return Columns.ToDictionary(z => z.Value, z => Values[z.Key].Trim(), StringComparer.CurrentCultureIgnoreCase);
            }
        }
    }

    /// <summary>
    /// A helper class to split delimited file lines in a standard way.
    /// </summary>
    public class DelimitedLineSplitter
    {
        /// <summary>
        /// The delimiter to use when splitting the line.
        /// </summary>
        public char Delimiter { get; set; }

        /// <summary>
        /// The text qualifier used, if any.
        /// </summary>
        public char? Qualifier { get; set; } = null;

        /// <summary>
        /// Creates a new instance of this class with the default delimiter (',') and no text qualifier.
        /// </summary>
        public DelimitedLineSplitter() : this(',', null)
        { }

        /// <summary>
        /// Creates a new instance of this class with the specified delimiter and no text qualifier.
        /// </summary>
        /// <param name="Delimiter">A char representing the column delimiter.</param>
        public DelimitedLineSplitter(char Delimiter) : this(Delimiter, null)
        { }

        /// <summary>
        /// Creates a new instance of this class with the specified delimiter and text qualifier.
        /// </summary>
        /// <param name="Delimiter">A char representing the column delimiter.</param>
        /// <param name="Qualifier">A char representing the text qualifier.</param>
        public DelimitedLineSplitter(char Delimiter, char? Qualifier)
        {
            this.Delimiter = Delimiter;
            this.Qualifier = Qualifier;
        }

        /// <summary>
        /// Splits the specified line. Uses the delimiter and qualifer specified in the constructor.
        /// </summary>
        /// <param name="Line">Delimited line to split.</param>
        /// <returns>Returns an IList&lt;string&gt; containing the column values.</returns>
        public List<string> SplitLine(string Line)
        {
            return SplitLine(Line, Delimiter, Qualifier);
        }

        /// <summary>
        /// Spluts the specified line. Uses the specified delimiter and text qualifier.
        /// </summary>
        /// <param name="Line">Delimited line to split.</param>
        /// <param name="Delimiter">A char representing the column delimiter.</param>
        /// <param name="Qualifier">A char representing the text qualifier. Optional.</param>
        /// <returns></returns>
        public List<string> SplitLine(string Line, char Delimiter, char? Qualifier = null)
        {
            if (Qualifier != null && Line.Contains(((char)Qualifier).ToString()))
            {
                var qualifier = (char)Qualifier;
                var split = Line.Split(Delimiter).ToList();
                for (int i = 0; i < split.Count; i++)
                {
                    if (split[i].StartsWith(qualifier.ToString()))
                    {
                        split[i] = split[i].Substring(1);
                        while (!split[i].EndsWith(qualifier.ToString()))
                        {
                            split[i] += Delimiter + split[i + 1];
                            split.RemoveAt(i + 1);
                        }
                        split[i] = split[i].Substring(0, split[i].Length - 1);
                    }
                }
                return split;
            }
            else
            {
                return Line.Split(Delimiter).ToList();
            }
        }
    }
}
