using System;
using System.Collections.Generic;
using System.Linq;

namespace ExtensionLibrary
{
    /// <summary>
    /// Calculates rates. Could be data transfered per second. Could be rows written per second. Could be files deleted per second. Simply record the count of total completed operations (bytes transferred, files deleted, etc).
    /// </summary>
    public class RateCalculator
    {
        /// <summary>
        /// A list of values and when they were recorded.
        /// </summary>
        private List<KeyValuePair<DateTime, long>> Values { get; }
        /// <summary>
        /// The total number of seconds during which to track values. The time elapsed between the oldest and newest values must be less than this value.
        /// </summary>
        public int SecondsToTrack { get; }
        /// <summary>
        /// The rate per second.
        /// </summary>
        public double RatePerSecond { get; set; } = 0;

        public RateCalculator(int SecondsToTrack = 10)
        {
            this.SecondsToTrack = SecondsToTrack;
             Values = new List<KeyValuePair<DateTime, long>>();
        }

        /// <summary>
        /// Add a new value to the value list. Purges old values outside of the desired tracking timeframe and calculates the per second rate.
        /// </summary>
        /// <param name="Value">The value to add to the list.</param>
        public void AddValue(long Value)
        {
            Values.Add(new KeyValuePair<DateTime, long>(DateTime.Now, Value));
            PurgeOldRecords();
            CalculateRate();
        }

        /// <summary>
        /// Purges any old records from the list. While the time between the first record and last is greater than SecondsToTrack, remove the first record.
        /// </summary>
        public void PurgeOldRecords()
        {
            while((Values.Last().Key - Values.First().Key).TotalSeconds > SecondsToTrack)
            {
                Values.RemoveAt(0);
            }
        }

        /// <summary>
        /// Calculates the rate. Takes the value of the last - value of the first. Divides by the total milliseconds x 1000 elapsed between first value entry and last.
        /// </summary>
        private void CalculateRate()
        {
            var last = Values.Last();
            var first = Values.First();

            var span = last.Key - first.Key;
            var count = last.Value - first.Value;

            RatePerSecond = ((double)count / span.TotalMilliseconds) * 1000;
        }
    }
}
