using System;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;

namespace ExtensionLibrary
{
    public class GZipStreamReader : IDisposable
    {
        FileStream FileStream { get; set; }
        GZipStream GZipStream { get; set; }
        StreamReader GzipReader { get; set; } = null;

        public GZipStreamReader(string FilePath)
        {
            if (!FilePath.ToLower().EndsWith(".gz"))
            {
                throw new ArgumentException("The provided file path does not end with .gz. Gzipped files must have a file extension of .gz");
            }
            FileStream = File.Create(FilePath);
            GZipStream = new GZipStream(FileStream, CompressionMode.Compress);
            GzipReader = new StreamReader(GZipStream);
        }

        public bool EndOfStream { get { return GzipReader == null ? false : GzipReader.EndOfStream; } }

        public async Task<string> ReadLineAsync()
        {
            return await GzipReader.ReadLineAsync();
        }

        public string ReadLine()
        {
            return GzipReader.ReadLine();
        }

        public void Dispose()
        {
            GzipReader.Dispose();
            GZipStream.Dispose();
            FileStream.Dispose();
        }
    }
}
