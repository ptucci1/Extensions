using FastMember;

namespace ExtensionLibrary
{
    internal class LineDelimiter
    {
        public string Delimiter { get; }
        public char Qualifier { get; }

        public LineDelimiter(string delimiter, char qualifier = '"')
        {
            Delimiter = delimiter;
            Qualifier = qualifier;
        }

        public string GetHeaders<T>()
        {
            return GetHeaders<T>(Delimiter, Qualifier);
        }

        public string GetHeaders<T>(string delimiter, char qualifier)
        {
            var properties = typeof(T).GetProperties();
            return ToLine(properties.Select(z => z.Name), delimiter, qualifier);
        }

        public string GetLine<T>(T t)
        {
            return GetLine(t, Delimiter, Qualifier);
        }

        public string GetLine<T>(T t, string delimiter, char qualifier)
        {
            var properties = typeof(T).GetProperties();
            var accessor = TypeAccessor.Create(typeof(T));
            return ToLine(properties.Select(z => accessor[t, z.Name]), delimiter, qualifier);
        }

        private string ToLine<T>(IEnumerable<T> objects, string delimiter, char qualifier)
        {
            return string.Join(delimiter, objects.Select(z => DelimitValue(z)));
        }

        private string DelimitValue<T>(T t)
        {
            if (t == null) return "";
            var s = t.ToString()!;
            if(s.Contains(Delimiter))
            {
                return $"{Qualifier}{s}{Qualifier}";
            }
            return s;
        }

        public IDictionary<string, string> GetRecord(string line, string[] columns)
        {
            return GetRecord(line, columns, Delimiter, Qualifier);
        }

        public IDictionary<string, string> GetRecord(string line, string[] columns, string delimiter, char qualifier)
        {
            var split = ParseLine(line, delimiter, qualifier);
            if (split.Count != columns.Count())
            {
                throw new Exception($"Column count ({columns.Length}) does not match item count ({split.Count}");
            }
            else
            {
                var result = new Dictionary<string, string>();
                for (int i = 0; i < columns.Length; i++)
                {
                    result.Add(columns[i], split[i]);
                }
                return result;
            }
        }

        public List<string> ParseLine(string line)
        {
            return ParseLine(line, Delimiter, Qualifier);
        }

        public List<string> ParseLine(string line, string delimiter, char qualifier)
        {
            var split = line.Split(delimiter).ToList();
            for (int i = 0; i < split.Count; i++)
            {
                //If current item starts with a qualfier, it's a quoted string.
                if (split[i].StartsWith(qualifier))
                {
                    //Start by removing qualifier from index 0 of the current item
                    split[i] = split[i].Substring(1);

                    //While the current item doesn't end with a qualifier, keep adding the next item.
                    while (!split[i].EndsWith(qualifier))
                    {
                        split[i] = $"{split[i]}{delimiter}{split[i + 1]}";
                        split.RemoveAt(i + 1);
                    }

                    //The current item now ends with a qualifier. Split it off.
                    split[i] = split[i].Substring(0, split[i].Length - 1);
                }
            }
            return split;
        }
    }
}
