using System;
using System.Diagnostics;

namespace ExtensionLibrary
{
    public class ThrottledProgress<T> : IProgress<T>
    {
        private Stopwatch Timer { get; }
        public IProgress<T> Progress { get; }
        public int ReportIntervalMs { get; }

        public ThrottledProgress(Action<T> ProgressAction, int ReportIntervalMs = 250) : this(new Progress<T>(ProgressAction), ReportIntervalMs)
        { }

        public ThrottledProgress(IProgress<T> Progress, int ReportIntervalMs = 250)
        {
            this.Progress = Progress;
            this.ReportIntervalMs = ReportIntervalMs;
            Timer = Stopwatch.StartNew();
        }

        public void Report(T value)
        {
            if(Timer.ElapsedMilliseconds >= ReportIntervalMs)
            {
                Timer.Restart();
                Progress.Report(value);
            }
        }
    }
}
