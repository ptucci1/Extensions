using ClosedXML.Excel;

namespace Extensions
{
    public class ExcelReader
    {
        public bool IgnoreCase { get; set; }

        public ExcelReader(bool ignoreCase = true)
        {
            IgnoreCase = ignoreCase;
        }

        public IEnumerable<IDictionary<string, object>> ReadSheet(Stream stream, int columnRow = 1, int firstDataRow = 2, int sheetNumber = 1)
        {
            using var workbook = new XLWorkbook(stream);
            var sheet = workbook.Worksheet(sheetNumber);
            var columns = GetColumns(sheet, columnRow).ToArray();

            foreach (var row in sheet.Rows().Skip(firstDataRow - 1))
            {
                var result = new Dictionary<string, object>(StringComparer.InvariantCultureIgnoreCase);
                for(int i = 0; i < columns.Length; i++)
                {
                    result.Add(columns[i], row.Cell(i + 1).Value.ToString());
                }
                yield return result;
            }
        }

        private IEnumerable<string> GetColumns(IXLWorksheet worksheet, int columnRow)
        {
            foreach (var cell in worksheet.Row(columnRow).Cells())
            {
                if (cell.TryGetValue<string>(out var s) && s != null)
                {
                    yield return s;
                }
                else
                {
                    break;
                }
            }
        }
    }
}

