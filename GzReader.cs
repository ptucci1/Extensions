using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;

namespace ExtensionLibrary
{
    public class GzReader : IDisposable
    {
        FileStream FileStream { get; set; }
        GZipStream GZipStream { get; set; }
        StreamReader StreamReader { get; set; } = null;

        public GzReader(string FilePath)
        {
            FileStream = File.Open(FilePath, FileMode.Open, FileAccess.Read);
            GZipStream = new GZipStream(FileStream, CompressionMode.Decompress);
            StreamReader = new StreamReader(GZipStream);
        }

        public bool EndOfStream { get { return StreamReader == null ? false : StreamReader.EndOfStream; } }

        public string ReadLine()
        {
            return StreamReader.ReadLine();
        }

        public async Task<string> ReadLineAsync()
        {
            return await StreamReader.ReadLineAsync();
        }

        public IEnumerable<string> ReadLines()
        {
            while (!StreamReader.EndOfStream)
            {
                yield return StreamReader.ReadLine();
            }
        }

        public async Task<IEnumerable<string>> ReadAllLinesAsync()
        {
            var lines = new List<string>();
            while(!StreamReader.EndOfStream)
            {
                lines.Add(await StreamReader.ReadLineAsync());
            }
            return lines;
        }

        public void Dispose()
        {
            StreamReader.Close();
            GZipStream.Close();
            FileStream.Close();
        }
    }
}
