using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ExtensionLibrary
{
    /// <summary>
    /// A class like StreamWriter that writes Dictionaries to file.
    /// </summary>
    /// <typeparam name="TKey">The Key type of the input Dictioanries.</typeparam>
    /// <typeparam name="TValue">The Value type of the input Dictionaries.</typeparam>
    public class DictionaryStreamWriter<TKey, TValue> : IDisposable
    {
        /// <summary>
        /// The internal StreamWriter that remains open for the life of the class.
        /// </summary>
        private StreamWriter StreamWriter { get; }
        /// <summary>
        /// The path to write the results.
        /// </summary>
        public string FilePath { get; }
        /// <summary>
        /// A count of rows written.
        /// </summary>
        public int RowWritten { get; private set; } = 0;
        /// <summary>
        /// The delimiter to use to split Dictionary items when writing to file.
        /// </summary>
        public char Delimiter { get; }

        /// <summary>
        /// Creates a new instance of DictionaryStreamWriter.
        /// </summary>
        /// <param name="FilePath">The path to the output file.</param>
        /// <param name="Delimiter">The delimiter to use. Default is tab (\t).</param>
        public DictionaryStreamWriter(string FilePath, char Delimiter = '\t')
        {
            StreamWriter = new StreamWriter(FilePath);
            this.FilePath = FilePath;
            this.Delimiter = Delimiter;
        }

        /// <summary>
        /// Writes the specified Dictionary to the output file.
        /// </summary>
        /// <param name="Line">The Dictionary to write.</param>
        public void WriteLine(IDictionary<TKey, TValue> Line)
        {
            if(RowWritten == 0)
            {
                WriteValues(Line.Keys);
            }
            WriteValues(Line.Values);
        }

        /// <summary>
        /// Writes a set of values to the output file. This method is generic to avoid boxing. If input type was a collection of object, all objects would be boxed, and would need to be unboxed before using.
        /// </summary>
        /// <typeparam name="T">The type of value to be written.</typeparam>
        /// <param name="Values">The values to be written.</param>
        private void WriteValues<T>(IEnumerable<T> Values)
        {
            var columns = Values.Select(z => z == null ? "" : z.ToString());
            var columnRow = string.Join(Delimiter.ToString(), columns);
            StreamWriter.WriteLine(columnRow);
            RowWritten++;
        }

        /// <summary>
        /// Disposes of this DictionaryStreamWriter.
        /// </summary>
        public void Dispose()
        {
            StreamWriter.Dispose();
        }
    }
}
