using System;
using System.Collections.Generic;
using System.Reflection;

namespace ExtensionLibrary
{
    public class ObjectFactory
    {
        private IDictionary<Type, IEnumerable<PropertyInfo>> Properties { get; set; }

        public T FromDictionary<T, TValue>(IDictionary<string, TValue> Item, bool AllowMappingFailures = false)
        {
            var properties = GetProperties(typeof(T));
            var t = Activator.CreateInstance<T>();
            foreach(var property in properties)
            {
                if(Item.TryGetValue(property.Name, out var value))
                {
                    property.SetValue(t, ChangeObjectType(value, property.PropertyType));
                }
                else
                {
                    if(!AllowMappingFailures)
                    {
                        throw new Exception("The dictionary does not contain the specified property value.");
                    }
                }
            }
            return t;
        }

        private IEnumerable<PropertyInfo> GetProperties(Type Type)
        {
            IEnumerable<PropertyInfo> properties;
            if(Properties.TryGetValue(Type, out properties))
            {
                return properties;
            }
            properties = Type.GetProperties();
            Properties.Add(Type, properties);
            return properties;
        }

        private object ChangeObjectType<T>(T t, Type Type)
        {
            if(t == null)
            {
                return t;
            }
            return Convert.ChangeType(t, Type);
        }
    }
}
