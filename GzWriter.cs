using System;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;

namespace ExtensionLibrary
{
    public class GzWriter : IDisposable
    {
        FileStream FileStream { get; set; }
        GZipStream GZipStream { get; set; }
        StreamWriter StreamWriter { get; set; } 

        public GzWriter(string Path)
        {
            if(!Path.ToLower().EndsWith(".gz"))
            {
                Path = Path + ".gz";
            }
            FileStream = new FileStream(Path, FileMode.CreateNew, FileAccess.Write);
            GZipStream = new GZipStream(FileStream, CompressionMode.Compress);
            StreamWriter = new StreamWriter(GZipStream);
        }

        public void WriteLine(string Line)
        {
            StreamWriter.WriteLine(Line);
        }

        public async Task WriteLineAsync(string Line)
        {
            await StreamWriter.WriteLineAsync(Line);
        }

        public void Close()
        {
            Dispose();
        }

        public void Dispose()
        {
            StreamWriter.Close();
            GZipStream.Close();
            FileStream.Close();
        }
    }
}
